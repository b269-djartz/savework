const Transaction = require("../models/Transaction");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//<<-------------EDIT TRANSACTIONS--------------->>\\
module.exports.editTransaction = async (data) => {
    if(data.isAdmin) {
    	const updatedStatus = {
    		status: data.reqBody.status
    	}
    	const transaction = await Transaction.findByIdAndUpdate(data.transactionId, updatedStatus, { new: true });
    	if (!transaction) {
    	  return "Transaction not found";
    	}
    	transaction.save()
    	return true;
    }
    const message = Promise.resolve('User must be ADMIN to access this!');
    return message.then((value) => {
    	return {value};
    });

    
};

//<<-------------ALL PENDING TRANSACTIONS--------------->>\\
module.exports.getPendingTransactions = async (data) => {
	if(data.isAdmin) {
		const pendingTransactions = await Transaction.find({status: "Pending"});
		if(pendingTransactions.length === 0) {
		return false;
		} else {
			return pendingTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------ALL SHIPPED OUT TRANSACTIONS--------------->>\\
module.exports.getShippedOutTransactions = async (data) => {
	if(data.isAdmin) {
		const shippedOutTransactions = await Transaction.find({status: "Shipped Out"});
		if(shippedOutTransactions.length === 0) {
		return false;
		} else {
			return shippedOutTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------ALL PENDING TRANSACTIONS--------------->>\\
module.exports.getDeliveredTransactions = async (data) => {
	if(data.isAdmin) {
		const deliveredTransactions = await Transaction.find({status: "Delivered"});
		if(deliveredTransactions.length === 0) {
		return false;
		} else {
			return deliveredTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------ALL TRANSACTIONS--------------->>\\
module.exports.getAllTransactions = async (data) => {
	if(data.isAdmin) {
		const allTransactions = await Transaction.find();
		if(allTransactions.length === 0) {
		return false;
		} else {
			return allTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};