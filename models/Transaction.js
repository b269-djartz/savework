const mongoose = require('mongoose');

const TransactionSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    totalAmount: {
        type: Number,
        required: true
    },
    subtotal: {
        type: Number,
        required: true
    },
    shippingFee: {
        type: Number,
        required: true
    },
    voucherDiscount: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    books: [{
        title: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
    }],
    paymentMethod: {
        type: String,
        enum: ['CashOnDelivery', 'E-wallet'],
        required: true
    }
});

module.exports = mongoose.model('Transaction', TransactionSchema);

