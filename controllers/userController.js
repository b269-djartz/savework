const User = require("../models/User");
const Book = require("../models/Book");
const Transaction = require("../models/Transaction");
const Voucher = require("../models/Voucher");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//<<-------------REGISTER---------------->>\\
module.exports.registerUser = async (reqBody) => {
  const existingUserEmail = await User.findOne({ email: reqBody.email });
  if (existingUserEmail) {
    return "Email already in use";
  };

  const existingUserUsername = await User.findOne({ username: reqBody.username });
  if (existingUserUsername) {
    return "Username already in use";
  };

  const newUser = new User({
    name: reqBody.name,
    username: reqBody.username,
    location: reqBody.location,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10)
  });

  return newUser.save().then((user, error) => {
    if (error) {
      return `Please ensure all fields are complete`;
    } else {
      return `Thank you for registering ${reqBody.name}!`;
    };
  });
};


//<<-------------USER AUTHENTICATION---------------->>\\
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

//<<-------------RETRIEVE USER DETAILS---------------->>\\
module.exports.getProfile = (data) => {
	return User.findById(data.userId,{isAdmin: 0}).then(result => {
		result.password = "";
		return result;
	});
};

//<<-------------SET USER AS ADMIN---------------->>\\
module.exports.setUserAsAdmin = (data) => {
	if(data.isAdmin) {
		const updateUserAsAdmin = {
		isAdmin: true
	};
	return User.findByIdAndUpdate(data.reqParams.userId, updateUserAsAdmin).then((course, error) => {
		if (error) {
			return false;
		} else {
			return `New admin created!`;
		};
	});
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------RETRIEVE USER TRANSACTION---------------->>\\
module.exports.getUserTransactions = async (data) => {
	const userTransactions = await Transaction.find({userId: data.userId});
	if(userTransactions.length === 0) {
		return false;
	} else {
		return userTransactions;
	}
};

//<<-------------RETRIEVE USER PENDING TRANSACTION---------------->>\\
module.exports.getUserPendingTransactions = async (data) => {
	const pendingTransactions = await Transaction.find({userId: data.userId,status: "Pending"});
	if(pendingTransactions.length === 0) {
		return false;
	} else {
		return pendingTransactions;
	}
};

//<<-------------RETRIEVE USER SHIPPED OUT TRANSACTION---------------->>\\
module.exports.getUserShippedOutTransactions = async (data) => {
	const shippedOutTransactions = await Transaction.find({userId: data.userId,status: "Shipped Out"});
	if(shippedOutTransactions.length === 0) {
		return false;
	} else {
		return shippedOutTransactions;
	}
};

//<<-------------RETRIEVE USER DELIVERED TRANSACTION---------------->>\\
module.exports.getUserDeliveredTransactions = async (data) => {
	const deliveredTransactions = await Transaction.find({userId: data.userId,status: "Delivered"});
	if(deliveredTransactions.length === 0) {
		return false;
	} else {
		return deliveredTransactions;
	}
};

//<<-------------RETRIEVE USER CANCELLED TRANSACTION---------------->>\\
module.exports.getUserCancelledTransactions = async (data) => {
	const cancelledTransactions = await Transaction.find({userId: data.userId,status: "Cancelled"});
	if(cancelledTransactions.length === 0) {
		return false;
	} else {
		return cancelledTransactions;
	}
};

//<<-------------CANCEL USER TRANSACTION---------------->>\\
module.exports.cancelTransaction = async (reqBody) => {
  const transaction = await Transaction.findById(reqBody.transactionId);

  if (!transaction) {
    return "Transaction not found";
  }

  if (transaction.status === "Shipped out" || transaction.status === "Delivered") {
    return "Cannot cancel transaction with status 'Shipped out' or 'Delivered'";
  }

  if (transaction.status !== "Pending") {
    return "Cannot cancel transaction with status other than 'Pending'";
  }

  transaction.status = "Cancelled";
  await transaction.save();

  // update product quantities and amounts sold
  for (let i = 0; i < transaction.books.length; i++) {
    const book = await Book.findOne({ title: transaction.books[i].title });

    if (!book) {
      return `Book with title ${transaction.books[i].title} not found`;
    }

    const quantity = transaction.books[i].quantity;
    book.stock += quantity;
    book.sold -= quantity;
    await book.save();
  }

  return "Transaction cancelled successfully";
};



module.exports.addVoucher = async (data) => {
	// Check if the user is an admin
	if (data.isAdmin) {
		// Create a new Book object using the Book model and the data provided
		const existingVoucher = await Voucher.findOne({ name: data.voucher.name, voucherName: data.voucher.voucherName  });
      if (existingVoucher) {
        return { success: false, message: "Voucher already exists!" };
      }

    const newVoucher = new Voucher({
			name: data.voucher.name,
			voucherName: data.voucher.voucherName,
			discount: data.voucher.discount
		});
		await newVoucher.save();
		return true
	};

	// If the user is not an admin, return a Promise with a message
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

















/*module.exports.getOrders = (reqParams) => {
	return User.findById(reqParams.userId, 'orders').then(result => {
		return result;
	});
};

module.exports.getAllOrders = (data) => {
	if(data.isAdmin) {
	return User.find(data.reqOrder,"orders").then(result => {
		return result;
	});	
	}
	
};*/


/*module.exports.checkout = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({bookId : data.bookId});
		return user.save().then((user,error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	});
	// Add user ID in the enrollees array of the course
	let isBookUpdated = await Book.findById(data.bookId).then(book => {
		book.buyers.push({userId: data.userId});
		return book.save().then((book,error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isBookUpdated) {
		return true;
	} else {
		return false;
	}




};*/
