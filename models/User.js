const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
username: {
  type: String,
  required: [true, "Fullname is required"]
},
name: {
  type: String,
  required: [true, "Fullname is required"]
},
location: {
  type: String,
  required: [true, "Location is required"]
},
email: {
  type: String,
  required: [true, "Email is required"]
},
  password: {
  type: String,
  required: [true, "Password is required"]
},
isAdmin: {
  type: Boolean,
  default: false
},
wishlist: {
  type: [String],
  default: []
}
  
});

module.exports = mongoose.model("User", userSchema);
